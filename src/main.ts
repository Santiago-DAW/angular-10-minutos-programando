// Este fichero es el que va a arrancar toda la aplicación de angular.
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';


// Esta línea arranca la aplicación a partir de AppModule (modulo principal).
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
