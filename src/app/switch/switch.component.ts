import { Component } from '@angular/core';

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.css']
})
export class SwitchComponent {
  opcionNumero: number;
  opcionString: string;

  constructor() {
    this.opcionNumero = 2;
    this.opcionString = 'uno';
  }
}
