import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { SaludoComponent } from './saludo/saludo.component';
import { PadreComponent } from './padre/padre.component';
import { Hijo1Component } from './hijo1/hijo1.component';
import { Hijo2Component } from './hijo2/hijo2.component';
import { EventosComponent } from './eventos/eventos.component';
import { AlertaComponent } from './alerta/alerta.component';
import { FormsModule } from '@angular/forms';
import { MainComponent } from './main/main.component';
import { SemaforoComponent } from './semaforo/semaforo.component';
import { BuclesComponent } from './bucles/bucles.component';
import { SwitchComponent } from './switch/switch.component';
import { SubrayadoDirective } from './directivas/subrayado.directive';

@NgModule({
  declarations: [
    AppComponent,
    SaludoComponent,
    PadreComponent,
    Hijo1Component,
    Hijo2Component,
    EventosComponent,
    AlertaComponent,
    MainComponent,
    SemaforoComponent,
    BuclesComponent,
    SwitchComponent,
    SubrayadoDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// Notas:
// Este fichero es la estructura de nuestra aplicación.
// Decorador: convierte a esta clase AppModule en un módulo de angular.
// declarations: array que contiene todos los elementos que forman parte de nuestra aplicación.
// directivas, pipes, componentes.
// AppComponet es el componente principal (raíz).
// imports: array que contiene los módulos externos a nuestra aplicación.
// modulos de angular o de terceras personas a incorporar dentro de nuestra aplicación.
// providers: array que contiene los servicios que usa nuestra aplicación.
// bootstrap: le decimos cuál es nuestro componente raíz a partir del cual vamos a empezar 
// a construir el resto de nuestra aplicación.
