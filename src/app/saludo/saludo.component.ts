import { Component } from '@angular/core';

@Component({
  selector: 'saludo',
  templateUrl: './saludo.component.html',
  styleUrls: ['./saludo.component.css']
})
export class SaludoComponent {
  mensaje: string;
  identifParrafo: string;
  identifInput: string;

  constructor() {
    this.mensaje = 'Un saludo a todos';
    this.identifParrafo = 'parrafoPrincipal';
    this.identifInput = 'text';

    setTimeout(() => {
      this.mensaje = 'Otro mensaje diferente';
      this.identifParrafo = 'main';
      this.identifInput= 'password';
    }, 6000);
  }

  mostrarSaludo(): string {
    return 'Saludo desde el método';
  }
}
