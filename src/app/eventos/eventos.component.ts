import { Component } from '@angular/core';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent {
  mensaje: string;

  constructor() {
    this.mensaje = 'Este es el mensaje inicial';
  }

  onClick($event: any): void {
    console.log('Se ha pulsado el boton');
  }

  onChange($event: any): void {
    console.log($event.target.value);
  }

  onMouseEnter(): void {
    this.mensaje = 'Estoy dentro del div';
  }

  onMouseOut(): void {
    this.mensaje = 'Saliendo del div';
  }

  // Imprime cuando pierde el foco sobre el imput.
  onChangeInput($event: any): void {
    // console.log($event.target.value);
  }

  // Imprime cada cambio introducido en el campo de texto (input).
  onInput($event: any): void {
    console.log($event.target.value);
    this.mensaje = $event.target.value;
  }

  // Al clicar/tabular sobre el input recibe el foco.
  onFocus(): void {
    console.log('Recibe el FOCO');
  }

  // Al clicar/tabular fuera del input pierde el foco.
  onBlur(): void {
    console.log('Pierde el FOCO');
  }
}
