import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  @Input() texto: string = 'undefined';

  // Los inputs no estan disponibles en el constructor.
  // En el constructor las propiedades de los inputs todavía 
  // no están definidas, no están disponibles.
  constructor() {
    console.log('**** Estoy en el constructor');
    console.log('Lo uso para inicializar propiedades');
    console.log(`TEXTO: ${this.texto}`);
  }

  // ngOnChanges: es donde se nos están modificando todas 
  // aquellas propiedades de tipo input que esén llegando 
  // desde el exterior.
  // Es el que nos permite recibir los valores de los inputs 
  // que reciba nuestro componente.
  // Este método tiene la capacidad de ejecutarse cada vez que 
  // cambian los inputs, las propiedades que le entran a nuestro 
  // componente.
  ngOnChanges(changes: SimpleChanges) {
    console.log('**** Estoy en el ngOnChanges');
    console.log(`TEXTO: ${this.texto}`);
    // changes: recibe todos los cambios de todos los inputs que 
    // recibe nuestro componente a lo largo de los diferentes 
    // cambios.
    console.log(`Valor anterior: ${changes['texto'].previousValue}`);
    console.log(`Valor actural: ${changes['texto'].currentValue}`);
  }

  ngOnInit(): void {
    // Acciones que arrancan el componente.
    console.log('**** Estoy en el ngOnInit');
    console.log('Lo uso para lanzar las acciones que arrancan el componente');
    console.log(`TEXTO: ${this.texto}`);
  }

  ngAfterViewInit() {
    // Todas aquellas acciones que tengamos que ejecutar una vez 
    // se esté mostrando ya la plantilla o el html.
    console.log('**** Estoy en el ngAfterViewInit');
    console.log('Lo uso para lanzar acciones iniciales dentro de la plantilla');
  }

  // Se ejecuta cuando el componente deja de estar visible en 
  // pantalla, cuadno deja de utilizarse.
  ngOnDestroy() {}
}
