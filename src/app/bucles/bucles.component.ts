import { Component } from '@angular/core';

@Component({
  selector: 'app-bucles',
  templateUrl: './bucles.component.html',
  styleUrls: ['./bucles.component.css']
})
export class BuclesComponent {
  personas: any[];

  constructor() {
    this.personas = [
      { nombre: 'Janice', apellidos: 'Najera Pabón', calle: 'Ctra. de Siles, 16', telefono: '741 385 960' },
      { nombre: 'Eliazar', apellidos: 'Estévez Carmona', calle: 'Avda. Rio Nalon, 7', telefono: '634 933 009' },
      { nombre: 'Apolo', apellidos: 'Loya Sierra', calle: 'Plazuela do Porto, 90', telefono: '783 326 373' },
      { nombre: 'Ascla', apellidos: 'Hernández Yánez', calle: 'Salzillo, 60', telefono: '684 653 495' },
      { nombre: 'Benet', apellidos: 'Betancourt Godoy', calle: 'Cañadilla, 47', telefono: '785 664 317' },
      { nombre: 'Winifred', apellidos: 'Cedillo Leyva', calle: 'Rúa do Paseo, 99', telefono: '693 673 215' },
      { nombre: 'Popea', apellidos: 'Lebrón Rangel', calle: 'Reiseñor, 38', telefono: '654 637 272' },
    ];
  }

  onClick() {
    this.personas.push({
      nombre: 'Mario', apellidos: 'Girón', calle: 'Gran Vía 21', telefono: '612 237 911'
    });
  }
}
